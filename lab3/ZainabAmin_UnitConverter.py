class UnitsConverter:
  def convert_to_units(self, num: int):
    kilobyte = num / 1000
    kibibyte = num / 1024
    megabyte = kilobyte / 1000
    mebibyte =  kibibyte / 1024

   
    print( " \nConversion Output: ")

    print(num, " bytes")
    print('%d kilobytes' %(kilobyte))
    print('%0.3f kibibytes' %(kibibyte))
    print('%0.1f megabytes' %(megabyte))
    print('%0.6f mebibytes' %(mebibyte))

while True:
  print("Enter a number:")
  input_var = input()
  if input_var.isnumeric():
    if int(input_var) > 0:
      UnitsConverter().convert_to_units(int(input_var))
      break
    else:
        print("Please enter a number greater than 0.")
  else: 
    print("Invalid input. Enter a numerical value that is greater than 0.")