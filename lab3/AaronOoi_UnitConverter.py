import ast #to use ast.literal_eval


#Unit Converter


def conversion(value):
    #The "{:,}" is to format the numbers for easy reading
    print("\nConversions:")
    print("{:,}".format(value), "bytes")
    print("{:,}".format(value/1000), "kilobytes")
    print("{:,}".format(value/1024), "kibibytes")
    print("{:,}".format(value/(1000*1000)), "megabytes")
    print("{:,}".format(value/(1024*1024)), "mebibytes")


#Evaluating the input before calling the function.
#If invalid input, program terminates with correct error value
#ast.literal_eval will know whether the input is a float or integer
val = input("Enter an integer in bytes: ")
if(not val.isdigit()):
    print("Please enter an integer only")
    quit()

#Cannot combine not isinstance() together with not val.isdigit() because cannot evaluate 
#val is an integer without calling ast.literal_eval(). But before calling ast.literal_eval(), need to make sure it is a number
val = ast.literal_eval(val)

if(val < 0):
    print("Please enter positive value")
elif(not isinstance(val,int)):
    print("Please enter integer only")
else:
    conversion(int(val))


