
#Python units converter

def convert_to_units(bytes):
    kiloBytes = bytes / 1000
    kibiBytes = bytes / 1024
    megaBytes = kiloBytes / 1000
    mebiBytes = kibiBytes /1024
    
    print(f"{bytes} bytes is equal to: ")
    print(f"{kiloBytes: .2f} kiloBytes (KiloB)")
    print(f"{kibiBytes: .2f} kibiBytes (KibiB)")
    print(f"{megaBytes :.2f} megaBytes (MegaB)")
    print(f"{mebiBytes: .2f} mebiBytes (MebiB)")

def main():
    while True:
        try:
    
            input_var = input("Please enter a positive number: ")
            bytes = int(input_var)

            if bytes < 0:
                 print("Enter a number greater than 0: ")
            elif bytes > 0:
                 convert_to_units(bytes)
                 break
        except ValueError:
            print ("Invalid! Enter a positive integer only!")

main()


            



 
            



        

 
