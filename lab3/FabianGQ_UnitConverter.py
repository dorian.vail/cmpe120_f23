''''
1) Error handling
2) class implementation w/ methods
3)formatting outputs
'''
class UnitsConverter:
    def convert_to_units(num : int):
        kiloByte = num / 1000
        kiBiByte = num / 1024
        megabyte = kiloByte / 1000
        meBiByte = kiBiByte / 1024
        print('%.2f kilobytes' %(kiloByte))
        print('%.2f kiBibytes' %(kiBiByte))
        print('%.4f megabytes' %(megabyte))
        print('%.4f meBibytes' %(meBiByte))

def main():
    while True:
        print("Enter a positive number: ")
        input_var = input()
        if (input_var.isnumeric()):
            input_var = int(input_var)
            if(input_var > 0):
                UnitsConverter.convert_to_units(input_var)
                break
            else:
                    print("Enter a positive number.")
        else:
            print("Entered nonnumeric value. Please enter a number.")

main()