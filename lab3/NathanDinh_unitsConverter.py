"""
Homework: Python Units Converter

Create a Python program that takes a number as input from the user and converts it into kilobytes (KB), kibibytes (kiB), megabytes (MB), and mebibytes (MiB).

1. Begin by prompting the user to enter a numerical value. Only integers are allowed (123), not floating point (1.23)

2. Convert the user's input into the following units:

Kilobytes (KB): 1 kilobyte is equal to 1000 bytes.
Kibibytes (kiB): 1 kibibyte is equal to 1024 bytes.
Megabytes (MB): 1 megabyte is equal to 1000 kilobytes.
Mebibytes (MiB): 1 mebibyte is equal to 1024 kibibytes.

3. Display the converted values along with appropriate labels to clearly indicate the unit of measurement (e.g., "X kilobytes," "Y kibibytes," etc.). You may use formatted output to make the results easy to read.

4. Ensure that your program handles errors gracefully. If the user enters invalid input (e.g., non-numeric input or negative values), provide a clear error message and allow the user to try again.

5. Test your program with various input values to ensure it produces correct and accurate conversions.

Written by Nathan Dinh
"""

class UnitsConverter():
    # Default constructor
    def __init__(self, number):
        self.number = number

    # Getters methods
    def getKiloByte(self):
        return self.number / 1000
    
    def getKiBiByte(self):
        return self.number / 1024

    def getMegaByte(self):
        return self.getKiloByte() / 1000
    
    def getMeBiByte(self):
        return self.getKiBiByte() / 1024

# main function
def main():
    isEndOfLoop = False
    while not isEndOfLoop:
        try:
            number = input("Enter a number or exit with 'q' >> ")
            
            if(number == "q"):
                isEndOfLoop = True
                print("Exit successfully!")
            elif(isinstance(int(number), int) and int(number) > 0):
                user = UnitsConverter(int(number))
                print("Here is the result:")
                print(number, "bytes")
                print(user.getKiloByte(), "kilobytes (KB)")
                print(user.getKiBiByte(), "kibibytes (kiB)")
                print(user.getMegaByte(), "megabytes (MB)")
                print(user.getMeBiByte(), "mebiBytes (miB)")
            else:
                print("Invalid Input. Please try again!")
        except:
            print("Not a number. Please try again!")

main()

