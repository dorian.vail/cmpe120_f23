#Name: Jasmin Sabry

# Write a function to return the uppercase version of a letter
def upper_case(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it is a lowercase letter
    if 97 <= letter_ascii <= 122:
        # make it uppercase
        letter_ascii -= 32
        # return the letter
        return chr(letter_ascii)

# Write a function to return the lowercase version of a letter
def lower_case(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it's a letter
    if 65 <= letter_ascii <= 90:
        # make it lowercase
        letter_ascii += 32
        # return the letter
        return chr(letter_ascii)

# Write a function that returns true if the letter is an alphabet
def alphabet(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it's a letter
    if (65 <= letter_ascii <= 90) or (97 <= letter_ascii <= 122):
        return True


# Write a function that returns true if an element is a digit
def digit(element):
    # get the ascii
    element_ascii = ord(element)
    # check if it's a digit
    if 48 <= element_ascii <= 57:
        return True

# Write a function to determine if the given character is a special character or not
def special_character(element):
    # get the ascii
    element_ascii = ord(element)
    # check if it is a special character
    if (32 <= element_ascii <= 47) or (58 <= element_ascii <= 64) or (
            91 <= element_ascii <= 96) or (123 <= element_ascii <= 126):
        return True

# Test cases
print(upper_case('j'))
print(lower_case('J'))
print(special_character('@'))
print(digit('5'))
print(alphabet('A'))