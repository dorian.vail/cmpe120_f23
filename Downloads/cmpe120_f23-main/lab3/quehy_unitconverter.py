# Python Unit Converter
# Define perform conversions between different units of data size
def convert_to_units(value):
    kilobytes = value * 1000
    kibibytes = value * 1024
    megabytes = value / 1000
    mebibytes = value / 1024
    return kilobytes, kibibytes, megabytes, mebibytes

# Use While for infite loop 
while True:
    # The user to enter some input
    user_input = input("Enter an integer: ")
    
    # Attempts to convert the user_input variable into an integer
    try:
        user_input = int(user_input)

        # If the user_input negative interger, the user_input have to input the positive integer
        if user_input < 0:
            print("Please enter a non-negative integer.")
        else:
            kb, kib, mb, mib = convert_to_units(user_input)
            
            # Print the result
            print(f"{user_input} is equal to:")
            print(f"{kb} kilobytes (KB)")
            print(f"{kib} kibibytes (kiB)")
            print(f"{mb} megabytes (MB)")
            print(f"{mib} mebibytes (MiB)")

            # Exit a loop prematurely based on a certain condition
            break

    except ValueError:
        print("Invalid input. Please enter an integer.")
