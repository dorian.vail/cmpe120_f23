
#1. Write a function to return the uppercase version of a letter
def uppercase(letter):
    uppercase = {
        'a': 'A', 'b': 'B', 'c': 'C', 'd': 'D', 'e': 'E',
        'f': 'F', 'g': 'G', 'h': 'H', 'i': 'I', 'j': 'J',
        'k': 'K', 'l': 'L', 'm': 'M', 'n': 'N', 'o': 'O',
        'p': 'P', 'q': 'Q', 'r': 'R', 's': 'S', 't': 'T',
        'u': 'U', 'v': 'V', 'w': 'W', 'x': 'X', 'y': 'Y',
        'z': 'Z'
    }
    return uppercase.get(letter, letter)

#Test CASE #1
print("Test CASE #1 return the uppercase version of a letter:")
result = uppercase('a')
print(result)  # Output: 'A'
result = uppercase('B')
print(result)  # Output: 'B'

#2. Write a function to return the lowercase version of a letter
def lowercase(letter):
    lowercase = {
        'A': 'a', 'B': 'b', 'C': 'c', 'D': 'd', 'E': 'e',
        'F': 'f', 'G': 'g', 'H': 'h', 'I': 'i', 'J': 'j',
        'K': 'k', 'L': 'l', 'M': 'm', 'N': 'n', 'O': 'o',
        'P': 'p', 'Q': 'q', 'R': 'r', 'S': 's', 'T': 't',
        'U': 'u', 'V': 'v', 'W': 'w', 'X': 'x', 'Y': 'y',
        'Z': 'z'
    }
    return lowercase.get(letter, letter)

#Test CASE #2
print("Test CASE #2 return the lowercase version of a letter:")
result = lowercase('A')
print(result)  # Output: 'a'
result = lowercase('b')
print(result)  # Output: 'b'

#3. Write a function that returns true if the letter is an alphabet
def is_alphabet(letter):
    return ('a' <= letter <= 'z') or ('A' <= letter <= 'Z')

#Test CASE #3
print("Test CASE #3 returns true if the letter is an alphabet")
result1 = is_alphabet('A')
print(result1)  # Output: True
result2 = is_alphabet('1')
print(result2)  # Output: False

#4. Write a function that returns true if an element is a digit
def is_digit(element):
    return '0' <= element <= '9'

#Test CASE #4
print("Test CASE #4 returns true if an element is a digit")
result1 = is_digit('5')
print(result1)  # Output: True
result2 = is_digit('x')
print(result2)  # Output: False

#5. Write a function to determine if the given character is a special character or not
def is_not_spec_char(char):
    return ('a' <= char <= 'z') or ('A' <= char <= 'Z') or ('0' <= char <= '9')

#Test CASE #5
print("Test CASE #5 determine if the given character is a special character or not")
result1 = is_not_spec_char('A')
print(result1)  # Output: True
result2 = is_not_spec_char('5')
print(result2)  # Output: True
result1 = is_not_spec_char('%')
print(result1)  # Output: False
result2 = is_not_spec_char('/')
print(result2)  # Output: False