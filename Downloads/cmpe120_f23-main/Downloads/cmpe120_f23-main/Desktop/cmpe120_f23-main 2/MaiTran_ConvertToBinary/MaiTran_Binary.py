def to_binary(number: int) -> str:
    """
    This is a comment
    Input: Number (integer)
    Output: String
    Example: Input = 10, Output = "0b1010"
    
    """
    if number == 0:
        return "0b0"

    binary_number = ""
    
    while number > 0:
        # Get the remainder using the % operator
        remainder = number % 2       
        # Prepend the remainder to the binary number
        binary_number = str(remainder) + binary_number   
         # Integer divide using the // operator
        number //= 2      
    
    return "0b" + binary_number

print(to_binary(6))  # Output should be: "0b0110"
print(to_binary(12))   # Output should be: "0b1100"
