ASCII_TABLE_DIFFERENCE = 32

#helper method to  determine if item is already lowercase
def isLower(letter : str) -> bool:
    return (letter in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'])

#using table linked on  Canvas, I used the difference to calculate whether I should add or subtract 32 to get the uppercase/lowercase version of the input letter
def uppercase(letter : str) -> str:
    if(isLower(letter)):
        asciiHex = ord(letter)
        return chr(asciiHex - ASCII_TABLE_DIFFERENCE)
    return letter



def lowercase(letter : str) -> str:
    if(not isLower(letter)):
        asciiHex = ord(letter)
        return chr(asciiHex + ASCII_TABLE_DIFFERENCE)
    return letter


def isAlphabet(letter : str) -> bool:
    letter = lowercase(letter)
    if letter not in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']:
        return False
    return True


def isDigit(num : str) -> bool:
    if num in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        return True
    return False


def isSpecial(input : str) -> bool:
    if(not isDigit(input) and not isAlphabet(input)):
        return True
    return False


print(uppercase('B'))
print(uppercase('b'))

print(lowercase('Z'))
print(lowercase('z'))

print(isAlphabet('b'))
print(isAlphabet('B'))
print(isAlphabet('^'))

print(isDigit('6'))
print(isDigit('b'))

print(isSpecial('b'))
print(isSpecial(":"))
print(isSpecial('.'))