"""
--------------PYTHON-ASCII---------------
Angie Do
Dr.Preetpal Kang 
CPME120

"""

# 1. Write a function to return the uppercase version of a letter
def uppercase_letter(letter1):
    letter_ascii = ord(letter1)
    if 97 <= letter_ascii <= 122:
        letter_ascii -= 32
    return chr(letter_ascii)

# 2. Write a function to return the lowercase version of a letter
def lowercase_letter(letter2):
    letter_ascii = ord(letter2)
    if 65 <= letter_ascii <= 90:
        return chr(ord(letter2) + 32)
    return letter2

# 3. Write a function that returns true if the letter is an alphabet
def is_alphabet(letter3):
    letter_ascii = ord(letter3)
    return (65 <= letter_ascii <= 90) or (97 <= letter_ascii <= 122)

# 4. Write a function that returns true if an element is a digit
def is_digit(letter4):
    letter_ascii = ord(letter4)
    return 48 <= letter_ascii <= 57

# 5. Write a function to determine if the given character is a special character or not
def is_specialCharacter(letter5):
    letter_ascii = ord(letter5)
    return (32 <= letter_ascii <= 47) or (58 <= letter_ascii <= 64) or (91 <= letter_ascii <= 96) or (123 <= letter_ascii <= 126)

# TEST
letter = input("Input the letter you want to change: ")
print("Uppercase:", uppercase_letter(letter))
print("Lowercase:", lowercase_letter(letter))

letter_to_check = input("Input the letter you want to check: ")
if is_alphabet(letter_to_check):
    print("This is an alphabet")
else:
    print("This is not an alphabet")

digit_to_check = input("Input the character you want to check: ")
if is_digit(digit_to_check):
    print("This is a digit")
else:
    print("This is not a digit")

char_to_check = input("Input the character you want to check: ")
if is_specialCharacter(char_to_check):
    print("This is a special character")
else:
    print("This is not a special character")