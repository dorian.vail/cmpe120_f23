"""
Homework: Python - ASCII

1. Write a function to return the uppercase version of a letter
2. Write a function to return the lowercase version of a letter
3. Write a function that returns true if the letter is an alphabet
4. Write a function that returns true if an element is a digit
5. Write a function to determine if the given character is a special character or not

Written by Nathan Dinh using Dictionary Data Structure
"""
# Letter are mapped with an ascii number in decimal
ascii_letters_table = {
    "A": 65, "B": 66, "C": 67, "D": 68, "E": 69, "F": 70, "G": 71,
    "H": 72, "I": 73, "J": 74, "K": 75, "L": 76, "M": 77, "N": 78,
    "O": 79, "P": 80, "Q": 81, "R": 82, "S": 83, "T": 84, "U": 85,
    "V": 86, "W": 87, "X": 88, "Y": 89, "Z": 90, "a": 97, "b": 98,
    "c": 99, "d": 100, "e": 101, "f": 102, "g": 103, "h": 104, "i": 105,
    "j": 106, "k": 107, "l": 108, "m": 109, "n": 110, "o": 111, "p": 112,
    "q": 113, "r": 114, "s": 115, "t": 116, "u": 117, "v": 118, "w": 119,
    "x": 120, "y": 121, "z": 120    
}

ascii_numbers_table = {
    "0": 48, "1": 49, "2": 50, "3": 51, "4": 52, "5": 53, "6": 54,
    "7": 55, "8": 56, "9": 57
}

# 1. Write a function to return the uppercase version of a letter
def to_uppercase(myLetter) -> str:
    uppercaseLetter = ""
    lowercaseDecimalValue = ascii_letters_table.get(myLetter)
    letterOffset = ascii_letters_table.get("a") - ascii_letters_table.get("A")

    if len(myLetter) > 1 or lowercaseDecimalValue == None:
        return "Your input is not a letter"
    elif lowercaseDecimalValue <= 90:
        return "Your letter is already uppercase"
    else:
        uppercaseLetterValue = lowercaseDecimalValue - letterOffset
        for letter in ascii_letters_table:
            if ascii_letters_table[letter] == uppercaseLetterValue:
                uppercaseLetter =  letter
                break

    return uppercaseLetter;

# Test cases
print("\n**************Test Cases for to_uppercase function**************")
print(to_uppercase("m")) # Should print "M"
print(to_uppercase("G")) # Should print "Your letter is already uppercase"
print(to_uppercase("aa")) # Should print "Your input is not a letter"
print(to_uppercase("2")) # Should print "Your input is not a letter"


# 2. Write a function to return the lowercase version of a letter
def to_lowercase(myLetter) -> str:
    lowercaseLetter = ""
    uppercaseDecimalValue = ascii_letters_table.get(myLetter)
    letterOffset = ascii_letters_table.get("a") - ascii_letters_table.get("A")

    if len(myLetter) > 1 or uppercaseDecimalValue == None:
        return "Your input is not a letter"
    elif uppercaseDecimalValue >= 97:
        return "Your letter is already lowercase"
    else:
        lowercaseLetterValue = uppercaseDecimalValue + letterOffset
        for letter in ascii_letters_table:
            if ascii_letters_table[letter] == lowercaseLetterValue:
                lowercaseLetter =  letter
                break

    return lowercaseLetter;

# Test cases
print("\n**************Test Cases for to_lowercase function**************")
print(to_lowercase("m")) # Should print "M"
print(to_lowercase("G")) # Should print "Your letter is already uppercase"
print(to_lowercase("aa")) # Should print "Your input is not a letter"
print(to_lowercase("2")) # Should print "Your input is not a letter"


# 3. Write a function that returns true if the letter is an alphabet
def isAlphabet(myLetter) -> bool:
    letterValue = ascii_letters_table.get(myLetter)
    if(letterValue == None):
        return False
    return True

# Test cases
print("\n**************Test Cases for isAlphabet function**************")
print(isAlphabet("m")) # Should print True
print(isAlphabet("G")) # Should print True
print(isAlphabet("aa")) # Shoul print False
print(isAlphabet("2")) # Should print False
print(isAlphabet("!")) # Should print False
print(isAlphabet("/")) # Should print False

# 4. Write a function that returns true if an element is a digit
def isDigit(myDigit) -> bool:
    digitValue = ascii_numbers_table.get(myDigit)
    if(digitValue == None):
        return False
    return True

# Test cases
print("\n**************Test Cases for isDigit function**************")
print(isDigit("m")) # Should print False
print(isDigit("G")) # Should print False
print(isDigit("aa")) # Shoul print False
print(isDigit("2")) # Should print True
print(isDigit("!")) # Should print False
print(isDigit("/")) # Should print False


# 5. Write a function to determine if the given character is a special character or not
def isSpecialChar(char):
    if len(char) > 1:
        return "Your input is not a character"
    else:
        charValue = ord(char)
        if(33 <= charValue <= 47 or 58 <= charValue <= 64 or 91 <= charValue <= 96 or 123 <= charValue <= 126):
            return True
    return False

# Test cases
print("\n**************Test Cases for isSpecialChar function**************")
print(isSpecialChar("m")) # Should print False
print(isSpecialChar("G")) # Should print False
print(isSpecialChar("aa")) # Shoul print "Your input is not a character"
print(isSpecialChar("2")) # Should print False
print(isSpecialChar("!")) # Should print True
print(isSpecialChar("/")) # Should print True

