"""
--------------PYTHON-Binary Converter---------------
Angie Do
Dr.Preetpal Kang 
CPME120

"""

def to_binary(number: int) -> str:
    """
    This function converts an integer to its binary representation.
    
    Input: Number (integer)
    Output: Binary representation (string)
    
    Example: Input = 255, Output = "0b11111111"
    """
    if number == 0:
        return "0b0"
    
    if number <0:
        return "Please try it again"

    answer = ""
    
    while number > 0:
        remainder = number % 2
        answer = str(remainder) + answer
        number //= 2
    
    return "0b" + answer

# Test cases for the to_binary function:
print(to_binary(0))     #output 0b0
print(to_binary(425))   #output 0b110101001
print(to_binary(900))   #output 0b1110000100
print(to_binary(70))    #output 0b1000110
print(to_binary(-1))    #Please try it again
