#HW: Phython - ASCII

# Description: write a function returning the uppercase of a letter
def to_upper(letter : str) -> str:
    #Check if param is an alpabet
    if is_alpabet == False:
        raise Exception("Given was not a letter")
    #Expected: turning lowercase to uppercase
    if ord(letter) >= 97 and ord(letter) <= 122:
        return chr(ord(letter) - 32)
    #occurs when letter is already capitalized
    else: 
        return letter

#Write a function to return the lowercase of a letter
def to_lower(letter :str) -> str:
    #Check if param is an alpabet
    if is_alpabet == False:
        raise Exception("Given was not a letter")
    #Expected: turning uppercase to lowercase
    if ord(letter) >= 65 and ord(letter) <= 90:
        return chr(ord(letter) + 32)
    #occurs when letter is already in lowercase
    else: 
        return letter

#Write a function that returns true if the letter if the letter is an alphabet
def is_alpabet(letter :str) -> bool:
    if (ord(letter) >= 65 and ord(letter) <= 90) or (ord(letter) >= 97 and ord(letter) <= 122):
        return True
    return False

#Write a function that returns true if an element is a digit
def is_digit(digit :str) -> bool:
    if ord(digit) >= 48 and ord(digit) <= 57:
        return True
    return False

#write a function to determine if the given character is a special character or not.
def is_a_special_character(char: str) -> bool:
    if (ord(char) >= 48 and ord(char) <= 57) or (ord(char) >= 65 and ord(char) <= 90) or (ord(char) >= 97 and ord(char) <= 122):
        return False
    return True

print(is_alpabet("A"))
print(is_digit("3"))
print(is_alpabet("*"))
print(is_digit("a"))
print(is_a_special_character("]"))
