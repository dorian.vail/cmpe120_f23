#Define a function to return the uppercase version of a letter
#Assuming the input is a letter
def uppercase_letter(char: str) -> str:
    if(ord(char) >= 65 and  ord(char) <= 90):
        return char
    else:
        return chr(ord(char) - 32)
    
#Define a function to return a lowercase version of a letter
#Assuming the input is a letter
def lowercase_ascii(char: str) -> str:
    if(ord(char) >= 97 and ord(char) <= 122):
        return char
    else:
        return chr(ord(char) + 32)

#Define a function that returns true if the letter is an alphabet
def is_an_alphabet(word: str) -> bool:
    if (ord(word) >= 65 and ord(word) <= 90) or (ord(word) >= 97 and ord(word) <= 122):
        return True
    return False

#Define a function that returns true if the letter is a digit
def is_a_digit(element: str) -> bool:
    if ord(element) >= 48 and ord(element) <= 57:
        return True
    return False  

#Define a function to determine if the given character is a special character or not
def is_a_special_character(char: str) -> bool:
    #Check if the input is a digit or letter. If it is, return false, otherwise return true
    if (ord(char) >= 48 and ord(char) <= 57) or (ord(char) >= 65 and ord(char) <= 90) or (ord(char) >= 97 and ord(char) <= 122):
        return False
    return True


print(uppercase_letter("a")) #output: A
print(uppercase_letter("T")) #output: T

print(lowercase_ascii("i")) #output: i
print(lowercase_ascii("Q")) #output: q

print(is_an_alphabet("t")) #output: True
print(is_an_alphabet("@")) #output: False

print(is_a_digit("k")) #output: False
print(is_a_digit("4")) #output: True

print(is_a_special_character("6")) #output: False
print(is_a_special_character("G")) #output: False
print(is_a_special_character("o")) #output: False
print(is_a_special_character("%")) #output: True