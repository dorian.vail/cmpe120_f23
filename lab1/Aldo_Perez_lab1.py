# HW: Python Binary Converter

def nibble_to_ascii(nibble: int) -> str:
    table = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    return table[nibble]


def to_hex(number: int) -> str:
    answer = ""
    # Forever loop
    while True:
        # Integer divide using the // operator
        quotient = number // 16
        # Get the remainder using the % operator
        remainder = number % 16

        # Accumulate result
        answer = nibble_to_ascii(remainder) + answer

        # Set the number we need to use for next time
        number = quotient

        # We break the "loop" when division turns to zero
        if (quotient == 0):
            break

    return "0x" + answer


# Test
print(to_hex(123456789))
print(to_hex(0b1010101))
print(to_hex(0xDEADBEEF))


def to_binary(number: int) -> str:
    answer = ""
    print("The number is " + str(number))
    # Forever loop
    while True:
        # Integer divide using the // operator
        quotient = number // 2
        # Get the remainder using the % operator
        remainder = number % 2

        # Accumulate result
        answer = nibble_to_ascii(remainder) + answer

        # Set the number we need to use for next time
        number = quotient

        # We break the "loop" when division turns to zero
        if (quotient == 0):
            break

    return "0b" + answer


# Test
print(to_binary(123456789))
print(to_binary(0b1010101))
print(to_binary(0xDEADBEEF))
