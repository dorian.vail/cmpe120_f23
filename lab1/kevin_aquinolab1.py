def decimal_to_binary(num):
  if (num <= 0):
    return '0b0'

  binary = ''
  #slightly altered while loop
  while (num > 0):
    remainder = num % 2
    binary = str(remainder) + binary
    num = num // 2

  return "0b" + binary


result = decimal_to_binary(25)
print(result)
